
library("e1071","LiblineaR")

load("Total_Data.RData")

Total_Data = data.frame(K = Total_Labels,Total_Features)

svm_model = svm(K~.,data=Training_Data_DF,kernel="linear",cachesize = 40000)

pred_result <- predict(svm_model, Testing_Data_DF[,-1])
table(Testing_Data_DF[,1],pred_result)

pred_result <- predict(svm_model, Training_Data_DF[,-1])
table(Training_Data_DF[,1],pred_result)

library(spls)
HAS_PVAR_COLS = which(apply(as.matrix(Training_Data_DF),2,var) > 0.1)
plsda_model = splsda(x=as.matrix(Training_Data_DF[,c(HAS_PVAR_COLS)][,-1]),y=Training_Data_DF[,1],K=20,eta=0.8,)

Testing_Data_DF[,c(HAS_PVAR_COLS)][,-1]
save(plsda_model,file="plsda_model_K20.RData")

pred_result <- predict( plsda_model, newx=Testing_Data_DF[,c(HAS_PVAR_COLS)][,-1])
table(Testing_Data_DF[,1],pred_result)


# Importants Coeffecients
cbind(colnames(Testing_Data_DF)[HAS_PVAR_COLS][-1][plsda_model$A],as.character(plsda_model$W[,1]))
levels(as.factor(do.call(cbind,lapply(strsplit(rownames(plsda_model$W),split="\\."),function(xx) xx[1]))))

levels(as.factor(do.call(cbind,lapply(strsplit(rownames(plsda_model$W),split="\\."),function(xx) xx[2]))))

# Importants Coeffecients


library(rpart)

rpart_model = rpart(K~.,data=Training_Data_DF, minsplit = 1,cp = 0.001)
plot(rpart_model)
text(rpart_model)

pred_rpart = predict(rpart_model, Testing_Data_DF[,-1])



cats_rpart_pred_Class <- apply( cats_rpart_pred,1,function(one_row) return(colnames(cats_rpart_pred)[which(one_row == max(one_row))]))
cats_Class_temp <- unclass(cats$Sex)
cats_Class <- attr(cats_Class_temp ,"levels")[cats_Class_temp]
table(cats_rpart_pred_Class,cats_Class)


library(spls)
HAS_PVAR_COLS = which(apply(as.matrix(Training_Data_DF),2,var) > 0.1)
plsda_model = splsda(x=as.matrix(Training_Data_DF[,c(HAS_PVAR_COLS)][,-1]),y=Training_Data_DF[,1],K=10,eta=0.9)
levels(as.factor(do.call(cbind,lapply(strsplit(rownames(plsda_model$W),split="\\."),function(xx) xx[1]))))



